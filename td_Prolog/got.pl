homme(jon).
parent(ned,jon).
femme(sansa).
femme(daenarys).
soeur(sansa,jon).
soeur(X,Y):-X\=Y, femme(X), memeParents(X,Y).
mariage(daenarys,jon).
pere(X,Y):-homme(X), parent(X,Y).
mere(X,Y):-femme(X), parent(X,Y).
epoux(X,Y):-homme(X), mariage(X,Y).
epouse(X,Y):-femme(X), mariage(X,Y).
couple(X,Y):-mariage(X,Y).
couple(X,Y):-mariage(Y,X).
fils(X,Y):-homme(X), parent(Y,X).
fille(X,Y):-femme(X), parent(Y,X).
enfant(X,Y):-fils(X,Y).
enfant(X,Y):-fille(X,Y).
grandPere(X,Y):-pere(X,Z), parent(Z,Y).
grandMere(X,Y):-mere(X,Z), parent(Z,Y).
grandParent(X,Y):-grandPere(X,Y).
grandParent(X,Y):-grandMere(X,Y).
petitFils(X,Y):-homme(X), grandParent(Y,X).
petiteFille(X,Y):-femme(X), grandParent(Y,X).
memePere(X,Y):-pere(Z,X), pere(Z,Y).
memeMere(X,Y):-mere(Z,X), mere(Z,Y).
memeParent(X,Y):-memePere(X,Y).
memeParent(X,Y):-memeMere(X,Y).
memeParents(X,Y):-memeMere(X,Y), memePere(X,Y).
frere(X,Y):-X\=Y, homme(X), memeParents(X,Y).
demiFrere(X,Y):-X\=Y, homme(X), memeParent(X,Y), \+memeParents(X,Y).
demiSoeur(X,Y):-X\=Y, femme(X), memeParent(X,Y), \+memeParents(X,Y).
oncle(X,Y):-parent(Z,Y), frere(Z,X).

tante(X,Y):-parent(Z,Y), soeur(Z,X).

neveu(X,Y):-homme(X), oncle(Y,X).
neveu(X,Y):-homme(X), tante(Y,X).
niece(X,Y):-femme(X), oncle(Y,X).
niece(X,Y):-femme(X), tante(Y,X).
cousin(X,Y):-neveu(X,Z), parent(Z,Y).
cousine(X,Y):-niece(X,Z), parent(Z,Y).
gendre(X,Y):-homme(X), couple(X,Z), fille(Z,Y).
bru(X,Y):-femme(X), couple(X,Z), fils(Z,Y).
belleMere(X,Y):-femme(X), couple(X,Z), pere(Z,Y), \+mere(X,Y).
beauPere(X,Y):-homme(X), couple(X,Z), mere(Z,Y), \+pere(X,Y).
ascendant(X,Y):-parent(X,Y).
ascendant(X,Y):-parent(X,Z), ascendant(Z,Y).
descendant(X,Y):-ascendant(Y,X).
lignee(X,Y):-ascendant(X,Y).
lignee(X,Y):-descendant(X,Y).
parente(X,Y):-ascendant(Z,X), ascendant(Z,Y).
