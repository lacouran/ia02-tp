

import copy


def where_player(world):
    for y in range(world.shape[0]):
        for x in range(world.shape[1]):
            if world[y][x] == -1:
                return [x, y]


def player_move_right(world, key, wasd):
    world=copy.deepcopy(world)
    [x, y] = where_player(world)

    return world