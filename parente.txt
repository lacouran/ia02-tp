pere(X,Y):-homme(X),parent(X,Y).
mere(X,Y):-parent(X,Y),femme(X).
epoux(X,Y):-homme(X),couple(X,Y).
epouse(X,Y):-femme(X),couple(X,Y).
couple(X,Y) :- epoux(X,Y);epouse(Y,X).
fils(X,Y) :- parent(Y,X),homme(X).
fille(X,Y):-parent(Y,X),femme(X).
enfant(X,Y):-parent(Y,X).
grand_mere(X,Y):-parent(X,Z),mere(Z,Y).
grand_pere(X,Y):-parent(X,Z),parent(Z,Y),homme(X).
grand_parent(X,Y):-grand_mere(X,Y),grand_pere(X,Y).
petit_fils(X,Y):-enfant(Y,Z),fils(Z,X).
petite_fille(X,Y):-enfant(Y,Z),fille(Z,X).
meme_Pere(X,Y):-homme(Z),parent(Z,X),parent(Z,Y),dif(X,Y).
meme_Mere(X,Y):-femme(Z),parent(Z,X),parent(Z,Y),dif(X,Y).
meme_Parent(X,Y):-meme_Pere(X,Y) or meme_Mere(X,Y).
frere(X,Y):-homme(X),meme_Parent(X,Y).
oncle(X,Y):-Parent(Z,Y),frere(X,Z).
tante(X,Y):-Parent(Z,Y),soeur(X,Z).
neveu(X,Y):-oncle(Y,X),homme(X).
neveu(X,Y):-tante(Y,X),homme(X).
niece(X,Y):-oncle(Y,X),femme(X).
niece(X,Y):-femme(X),oncle(Y,X).
cousin(X,Y):-oncle(Z,Y),fils(X,Z).
cousin(X,Y):-tante(Z,Y),fille(X,Z).
gendre(X,Y):-fille(Z,Y),epoux(X,Z).
bru(X,Y):fils(Z,Y),epouse(X,Z).
maratre(X,Y):pere(Z,Y).epouse(X,Z)\+(mere(X,Y)).
belle_mere(X,Y):-gendre(Y,X),femme(X).
belle_mere(X,Y):-bru(Y,X),femme(X).
ascendant(X,Y):-parent(X,Y).
ascendant(X,Y):-ascendant(X,Z),parent(X,Z).
descendant(X,Y):-ascendant(Y,X).
ligne(X,Y):-ascendant(X,Y),descendant(X,Y).
parente(X,Y):-ascendant(Z,X),ascendant(Z,Y),diff(X,Y).

    
