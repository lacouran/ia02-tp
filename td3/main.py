"""
[IA02] TP SAT/Sudoku template python
author:  Sylvain Lagrue
version: 1.0.2
"""
from pprint import pprint
from typing import List, Tuple
import subprocess
import itertools
# alias de types
Variable = int
Literal = int
Clause = List[Literal]
Model = List[Literal]
Clause_Base = List[Clause]
Grid = List[List[int]]

example: Grid = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],
    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],
    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9],
]


example2: Grid = [
    [0, 0, 0, 0, 2, 7, 5, 8, 0],
    [1, 0, 0, 0, 0, 0, 0, 4, 6],
    [0, 0, 0, 0, 0, 9, 0, 0, 0],
    [0, 0, 3, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 5, 0, 2, 0],
    [0, 0, 0, 8, 1, 0, 0, 0, 0],
    [4, 0, 6, 3, 0, 1, 0, 0, 9],
    [8, 0, 0, 0, 0, 0, 0, 0, 0],
    [7, 2, 0, 0, 0, 0, 3, 1, 0],
]


empty_grid: Grid = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
]

#### fonctions fournies


def write_dimacs_file(dimacs: str, filename: str):
    with open(filename, "w", newline="") as cnf:
        cnf.write(dimacs)




# converts given coords and field value to SAT value
# x,y in [0,8], n in [1,9]
dictClauses={}
def i(x,y,n):
    value = (x + y * 9) * 9 + n
    if value not in dictClauses:
        dictClauses[value] = {"x": x, "y": y,"value":n}
    return value

    # generates standard sudoku rules
# x, y coordonate
#


def generateCNFfromStartCondition(cnf,grid):
    for element in enumerate
        cnf.append((i(x,y,n)))








def write_cnf(cnf, filename):

    variables = max(map(abs, itertools.chain(*cnf)))
    cnf_str = '\n'.join(map(lambda c: ' '.join(map(str, c)) + ' 0', cnf))


    print('CNF created, it has %d variables and %d clauses' %
          (variables, len(cnf)))

def gen_sudoku_basis(cnf):
        # au moins un nombre par case
        for y in range(9):
            for x in range(9):
                t = []
                for n in range(1, 10):
                    t.append(i(x, y, n))
                cnf.append(t)

        # seulement 1
        for y in range(9):
            for x in range(9):
                for n in range(1, 10):
                    for k in range(1, 10):
                        if n != k:
                            cnf.append([-i(x, y, n), -i(x, y, k)])

        # 1 nombre par colonne
        for y in range(9):
            for n in range(1, 10):
                t = []
                for x in range(9):
                    t.append(i(x, y, n))
                cnf.append(t)

        # 1 nombre par ligne
        for x in range(9):
            for n in range(1, 10):
                t = []
                for y in range(9):
                    t.append(i(x, y, n))
                cnf.append(t)

        # 1 nombre par block
        for x in range(3):
            for y in range(3):
                for n in range(1, 10):
                    t = []
                    for dy in range(3):
                        for dx in range(3):
                            t.append(i(3 * x + dx, 3 * y + dy, n))
                    cnf.append(t)



def exec_gophersat(
    filename: str, cmd: str = "gophersat", encoding: str = "utf8"
) -> Tuple[bool, List[int]]:
    result = subprocess.run(
        [cmd, filename], capture_output=True, check=True, encoding=encoding
    )
    string = str(result.stdout)
    lines = string.splitlines()

    if lines[1] != "s SATISFIABLE":
        return False, []

    model = lines[2][2:].split(" ")

    return True, [int(x) for x in model]



def main():


    cnf = []

    gen_sudoku_basis(cnf)
    print(cnf)
    # print(dictClauses)


if __name__ == "__main__":
    main()